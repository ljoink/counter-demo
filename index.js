const express = require("express");
const fs = require("fs-extra");

const app = express();

let counter = 1;
app.get("/count", (req, res) => {
  const header = JSON.stringify(req.headers);
  const query = JSON.stringify(req.query);
  const params = JSON.stringify(req.params);

  //   {"host":"www.formalform.com","connection":"close","user-agent":"Amazon/EventBridge/ApiDestinations","│
  // range":"bytes=0-1048575","content-type":"application/json; charset=utf-8","accept-encoding":"gzip,def│
  // late"}
  const str = `${new Date()}: ${counter.toString()}\n${header}\n${query}\n${params}\n\n`;

  fs.appendFile("count.txt", str, (err) => {
    if (err) throw err;
    counter++;
  });

  res.send("Hello World");
});

app.listen(3000, () => {
  console.log("starting...");
});
